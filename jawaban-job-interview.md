# 1
![14-07-23](WhatsApp_Image_2023-07-31_at_18.32.23__1_.png)

# 2
![15-07-23](WhatsApp_Image_2023-07-31_at_18.32.23.png)

# 3
![16-07-23](WhatsApp_Image_2023-07-31_at_18.32.24__1_.png)

# 4
![16-07-23](WhatsApp_Image_2023-07-31_at_18.32.24__2_.png)

# 5
![16-07-23](WhatsApp_Image_2023-07-31_at_18.32.24.png)



# Nomor 1

Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

**Jawaban**

**Bahasa Java**

```java

    public void play() {
        System.out.println("Playing video: " + this.getTitle());
        this.views++;
        System.out.println("Views: " + views);
    }

    // Metode untuk mendapatkan jumlah tampilan video
    public int getViews() {
        return views;
    }

    public void addComment(String comment) {
        this.comments.add(comment);
    }

    public ArrayList<String> getComments() {
        return comments;
    }

    public void like() {
        System.out.println("Liking video: " + this.getTitle());
        this.likes++;
        System.out.println("Likes: " + likes);
    }

    public int getLikes() {
        return likes;
    }
}
```
**pendekatan matematika**

Program di bawah menggunakan konsep increment untuk menghitung jumlah tampilan dan jumlah suka dari video TikTok.

Dengan menggunakan operator increment `++`, setiap kali metode `play()` dipanggil, nilai tampilan (`views`) akan bertambah 1. Begitu juga dengan metode `like()`, setiap kali dipanggil, nilai suka (`likes`) akan bertambah 1. 
![Nomor 1](No._1.gif)

**Algoritma**

**Algoritma Deskriptif**

Algoritma pada program di atas menggambarkan beberapa metode yang terkait dengan objek video dalam sebuah program. 

1. Metode play() adalah metode yang digunakan untuk memutar video. Ketika metode ini dipanggil, judul video akan dicetak untuk memberikan informasi tentang video yang sedang diputar. Selanjutnya, jumlah tampilan (views) video akan ditingkatkan sebanyak 1. Setelah itu, metode akan mencetak jumlah tampilan terbaru dari video.

2. Metode getViews() adalah metode yang digunakan untuk mendapatkan jumlah tampilan video. Metode ini mengembalikan nilai dari variabel views, yang merepresentasikan jumlah tampilan video tersebut. 

3. Metode addComment(String comment) digunakan untuk menambahkan komentar baru ke video. Komentar tersebut akan dimasukkan ke dalam ArrayList comments, yang berfungsi sebagai wadah untuk menyimpan daftar komentar pada video. 

4. Metode getComments() berguna untuk mendapatkan daftar komentar yang telah ditambahkan pada video. Metode ini mengembalikan ArrayList comments, yang berisi daftar komentar-komentar pada video tersebut.

5. Metode like() adalah metode yang digunakan untuk memberikan suka pada video. Ketika metode ini dipanggil, judul video akan dicetak untuk memberikan informasi tentang video yang sedang diberi suka. Selanjutnya, jumlah suka (likes) video akan ditingkatkan sebanyak 1. 

6. Metode getLikes() digunakan untuk mendapatkan jumlah suka yang diberikan pada video. Metode ini mengembalikan nilai dari variabel likes, yang merepresentasikan jumlah suka video tersebut. 



# Nomor 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

[Algoritma EditVideo](https://gitlab.com/lahitanianissa32/project-tiktokpbo/-/blob/main/Edit)

masalah lain yang mungkin muncul adalah
Keterbatasan fitur: Algoritma ini hanya mencakup beberapa fitur dasar seperti efek, kecepatan, filter, beautify, dan mirror. Jika ingin mengembangkan editor video yang lebih canggih, mungkin perlu menambahkan lebih banyak opsi menu dan tindakan yang sesuai.

Solusinya mungkin dilakukan kembali analisis kebutuhan yang mendalam untuk menentukan fitur tambahan apa yang perlu ditambahkan. Identifikasi kebutuhan pengguna potensial, tren industri, dan fungsi-fungsi yang diharapkan dari editor video yang lebih canggih. Dengan pemahaman yang jelas tentang apa yang diinginkan oleh pengguna, Anda dapat menentukan fitur-fitur yang harus ditambahkan.

Tetapi kembali lagi hal ini membutuhkan waktu dan sumber daya yang cukup besar untuk melakukan analisis kebutuhan yang mendalam. Analisis yang komprehensif memerlukan penelitian, survei, pengujian pengguna, dan interaksi dengan pengguna potensial. Proses ini dapat memakan waktu dan tenaga yang signifikan. Tetapi tidak bisa dipungkiri mungkin kedepannya dapat diperbaiki lagi. 




# Nomor 3

Mampu menjelaskan konsep dasar OOP

**Jawaban**

1. Encapsulation (Pengkapsulan): Konsep pengkapsulan melibatkan penyembunyian atribut dan metode yang ada di dalam objek sehingga hanya dapat diakses melalui antarmuka publik yang ditentukan oleh kelas. Ini membantu dalam menerapkan konsep informasi tersembunyi dan memastikan integritas data.

2. Pewarisan (Inheritance): Pewarisan memungkinkan pembuatan kelas baru (kelas turunan) dengan mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass). Kelas turunan dapat menambahkan perilaku baru atau memodifikasi perilaku yang sudah ada.

3. Polimorfisme (Polymorphism): Polimorfisme memungkinkan penggunaan metode dengan nama yang sama di berbagai kelas. Metode dengan nama yang sama dapat diimplementasikan secara berbeda dalam setiap kelas, sehingga memberikan fleksibilitas dan modularitas dalam pengembangan perangkat lunak.

4. Abstraksi (Abstraction): Abstraksi melibatkan penyederhanaan kompleksitas sistem dengan memfokuskan pada aspek-aspek yang penting dan mengabaikan yang tidak penting. Abstraksi memungkinkan pembuatan kelas abstrak yang tidak dapat diinstansiasi dan hanya digunakan sebagai kerangka untuk kelas turunannya.

Pradigrama Objek Pemrograman Berorientasi Objek atau OOP ini banyak digunakan dalam dunia industri. Karena memungkinkan pemrograman yang modular, reusabilitas dan peningkatan keamanan serta memudahkan kolaborasi tim yang efektif.

# Nomor 4

Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

![Nomor 4](No._4_Terbaru.gif)

[Encapsulation](https://gitlab.com/lahitanianissa32/project-tiktokpbo/-/blob/main/Class%20VideoTiktok)

# Nomor 5

Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)

![Nomor 5](No._5_Terbaru.gif)

[Abstraction](https://gitlab.com/lahitanianissa32/project-tiktokpbo/-/blob/main/Class%20AVideo)


# Nomor 6

Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

![Nomor 6](No._6A_Terbaru.gif)
![Nomor 6](No._6B1_Terbaru.gif)

[Inheritence](https://gitlab.com/lahitanianissa32/project-tiktokpbo/-/blob/main/Class%20VideoTiktok)

[Polymorphism Override](https://gitlab.com/lahitanianissa32/project-tiktokpbo/-/blob/main/Class%20VideoTiktok)



# Nomor 7

Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

**Jawaban**

- Pertama-tama saya membuat gambaran skema dan proses bisnis yang akan saya buat.
- Identifikasi aktor-aktor yang terlibat dalam proses bisnis, seperti pengguna, sistem, dan sebagainya.
- Identifikasi use case-use case yang terkait dengan proses bisnis, seperti login, membuat video, mengirim pesan, dan sebagainya.
- Identifikasi objek-objek yang terlibat dalam proses bisnis, seperti penngguna, video, reaksi dan sebagainya.
- Membuat kelas-kelas(Class Diagram digunakan untuk pemodelan OOP) yang merepresentasikan aktor, use case, dan objek yang telah diidentifikasi dalam proses bisnis.
- Hubungkan kelas-kelas tersebut dengan relasi objek-objek, seperti kelas TiktokVideo yang memiliki hubungan dengan kelas Video.
- Implementasikan method-method yang dibutuhkan dalam setiap kelas, seperti method untuk membuat video, mengedit video, membeli barang, dan sebagainya.
- Uji coba model OOP yang telah dibuat untuk memastikan konsistensi dan fungsionalitasnya.


# Nomor 8

Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

**Jawaban**

**Class Diagram**

![Class Diagram TikTok](Untitled_Diagram-Page-3.drawio__5_.png)

| No. | Use Case                                                    | Nilai Prioritas | Aktor                    | Tersedia |
|-----|------------------------------------------------------------|-----------------|--------------------------|----------|
| 1   | Pembuatan dan Pengunggahan konten                           | 10              | Pengguna                 | Yes      |
| 2   | Penayangan konten                                           | 10              | Pengguna                 | Yes      |
| 3   | Fitur reaksi (Like video)                                   | 9               | Pengguna                 | Yes      |
| 4   | Fitur reaksi (Comment video)                                | 9               | Pengguna                 | Yes      |
| 5   | Fitur reaksi (Save video)                                   | 9               | Pengguna                 | Yes      |
| 6   | Fitur reaksi (Share video)                                  | 9               | Pengguna                 | Yes      |
| 7   | Fitur Live                                                  | 8               | Pengguna                 | Yes      |
| 8   | Fitur Pembelian barang di Tiktok shop                       | 8               | Pengguna                 | Yes      |
| 9   | Penggunaan filter video                                     | 8               | Pengguna                 | Yes      |
| 10  | Penggunaan efek video                                       | 8               | Pengguna                 | Yes      |
| 11  | Mengelola preferensi konten                                 | 8               | Pengguna                 | No       |
| 12  | Fitur Pencarian dan Penemuan Konten                         | 8               | Pengguna                 | No       |
| 13  | Menambahkan teks deskripsi                                  | 7               | Pengguna                 | Yes      |
| 14  | Fitur Duet                                                  | 7               | Pengguna                 | No       |
| 15  | Membuat konten dengan musik tertentu                        | 7               | Pengguna                 | No       |
| 16  | Mengikuti akun pengguna lain                                | 7               | Pengguna                 | No       |
| 17  | Mengelola akun dan profil pengguna                          | 7               | Pengguna                 | No       |
| 18  | Mengelola pesan dan percakapan                              | 7               | Pengguna                 | No       |
| 19  | Fitur melihat riwayat pembelian di Tiktok shop              | 7               | Pengguna                 | Yes      |
| 20  | Fitur tambah saldo di Tiktok shop                           | 6               | Pengguna                 | Yes      |
| 21  | Influencer marketing                                         | 6               | Pengguna                | No       |
| 22  | Menambahkan stiker dan emoji video                           | 6               | Pengguna                 | No       |
| 23  | Menonton video dengan hastag                                | 6               | Pengguna                 | No       |
| 24  | Tambahkan keranjang kuning                                   | 6               | Pengguna                 | Yes      |
| 25  | Fitur Kolaborasi                                            | 6               | Pengguna                 | No       |
| 26  | Login                                                       | 6               | Pengguna                 | Yes      |
| 27  | Logout                                                      | 6               | Pengguna                 | Yes      |
| 28  | Penambahan tagar pada video                                 | 5               | Pengguna                 | No       |
| 29  | Menandai teman di komentar atau deskripsi                    | 5               | Pengguna                 | No       |
| 30  | Menghapus komentar                                          | 5               | Pengguna                 | No       |
| 31  | Memblokir pengguna                                          | 5               | Pengguna                 | No       |
| 32  | Mengelola notifikasi                                        | 5               | Pengguna                 | No       |
| 33  | Fitur berbagi video dalam pesan                             | 5               | Pengguna                 | No       |
| 34  | Mengatur pengaturan suara                                   | 5               | Pengguna                 | No       |
| 35  | Pengelolaan konten yang melanggar kebijakan komunitas        | 9               | Manajemen                | No       |
| 36  | Analisis data pengguna dan aktivitas                        | 9               | Manajemen                | No       |
| 37  | Pemantauan konten dan penegakan hukum                        | 9               | Manajemen                | No       |
| 38  | Peningkatan kecepatan dan performa platform                  | 9               | Manajemen                | No       |
| 39  | Optimasi fitur privasi dan keamanan                         | 9               | Manajemen                | No       |
| 40  | Pengembangan dan pengujian fitur baru                       | 9               | Manajemen                | No       |
| 41  | Survei dan riset pasar                                      | 8               | Manajemen                | No       |
| 42  | Pengelolaan sistem filter                                    | 8               | Manajemen                | No       |
| 43  | Analisis data pengguna dan aktivitas untuk iklan terarget    | 8               | Manajemen                | No       |
| 44  | Peningkatan algoritma rekomendasi konten                     | 8               | Manajemen                | No       |
| 45  | Pengelolaan masukan dan umpan balik pengguna                | 8               | Manajemen                | No       |
| 46  | Pengelolaan kata sandi                                      | 5               | Manajemen                | No       |
| 47  | Kemitraan dengan agensi periklanan                           | 7               | Manajemen                | No       |
| 48  | Manajemen hubungan dan kemitraan dengan merek, selebriti, dan influencer | 7 | Manajemen | No |
| 49  | Pengelolaan program penghargaan dan promosi                  | 7               | Manajemen                | No       |
| 50  | Kampanye pemasaran dan promosi                              | 7               | Manajemen                | No       |
| 51  | Pengelolaan sistem pembayaran dan komisi                     | 6               | Manajemen                | No       |
| 52  | Penyelenggaraan acara dan kompetisi                          | 6               | Manajemen                | No       |
| 53  | Dukungan pelanggan                                          | 6               | Manajemen                | No       |
| 54  | Mengelola komunitas dan aturan perilaku                      | 5               | Manajemen                | No       |
| 55  | Peningkatan keamanan platform                               | 5               | Manajemen                | No       |
| 56  | Penanganan laporan pelanggaran                              | 5               | Manajemen                | No       |
| 57  | Pengelolaan pemulihan                                       | 7               | Manajemen                | No       |
| 58  | Pembaruan kebijakan dan persyaratan                          | 5               | Manajemen                | No       |
| 59  | Analisis kinerja platform TikTok                            | 9               | Direksi Perusahaan       | No       |
| 60  | Pengembangan strategi pertumbuhan dan ekspansi bisnis       | 9               | Direksi Perusahaan       | No       |
| 61  | Pengawasan dan pengendalian keuangan perusahaan              | 9               | Direksi Perusahaan       | No       |
| 62  | Pengambilan keputusan strategis untuk pengembangan produk   | 9               | Direksi Perusahaan       | No       |
| 63  | Pelaporan dan komunikasi kepada pemangku kepentingan         | 9               | Direksi Perusahaan       | No       |
| 64  | Riset pasar dan analisis tren konsumen                      | 8               | Direksi Perusahaan       | No       |
| 65  | Pengembangan kemitraan strategis                             | 8               | Direksi Perusahaan       | No       |
| 66  | Membuat dan melaksanakan kebijakan perusahaan                | 8               | Direksi Perusahaan       | No       |
| 67  | Pengelolaan risiko dan kepatuhan regulasi                    | 8               | Direksi Perusahaan       | No       |
| 68  | Mengawasi dan memantau pelaksanaan rencana bisnis            | 8               | Direksi Perusahaan       | No       |
| 69  | Pengembangan dan pelaksanaan strategi pemasaran              | 7               | Direksi Perusahaan       | No       |
| 70  | Evaluasi dan pemantauan performa departemen dan tim kerja    | 7               | Direksi Perusahaan       | No       |
| 71  | Mengelola hubungan dengan pemegang saham dan investor        | 7               | Direksi Perusahaan       | No       |
| 72  | Identifikasi dan penanganan tantangan bisnis                 | 7               | Direksi Perusahaan       | No       |
| 73  | Penyusunan dan pemantauan anggaran perusahaan                | 7               | Direksi Perusahaan       | No       |
| 74  | Pemantauan dan pelaporan indikator kinerja keuangan          | 7               | Direksi Perusahaan       | No       |
| 75  | Pelaksanaan program pelatihan dan pengembangan karyawan      | 6               | Direksi Perusahaan       | No       |
| 76  | Mengelola hubungan dengan lembaga regulasi dan pemerintah    | 6               | Direksi Perusahaan       | No       |
| 77  | Menetapkan visi, misi, dan tujuan perusahaan                 | 6               | Direksi Perusahaan       | No       |
| 78  | Pemantauan dan evaluasi pelaksanaan tanggung jawab sosial    | 6               | Direksi Perusahaan       | No       |
| 79  | Pengembangan dan pelaksanaan kebijakan SDM                   | 6               | Direksi Perusahaan       | No       |
| 80  | Pengelolaan kepemilikan intelektual                          | 6               | Direksi Perusahaan       | No       |
| 81  | Pengawasan dan pengendalian operasional perusahaan           | 5               | Direksi Perusahaan       | No       |
| 82  | Evaluasi dan pemantauan implementasi inisiatif perusahaan    | 5               | Direksi Perusahaan       | No       |
| 83  | Pemantauan dan evaluasi kinerja vendor dan mitra             | 5               | Direksi Perusahaan       | No       |
| 84  | Pelaksanaan kebijakan privasi dan keamanan data               | 5               | Direksi Perusahaan       | No       |



Dalam tabel tersebut, kolom "Tersedia" menunjukkan apakah use case tersebut tersedia atau tidak dalam program TikTok. "Yes" berarti use case tersebut tersedia, sementara "No" berarti tidak tersedia.

# Nomor 9

Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

**Jawaban**

https://youtube.com/watch?v=TFICJQXzewQ&feature=share7

# Nomor 10

Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

**Jawaban**

![screenshot bagian 1](No_10_bagian_1.jpeg)

![screenshot bagian 2](No_10_bagian_2.jpeg)

![screenshot bagian 3](No_10_bagian_3.jpeg)

![screenshot bagian 4](No_10_bagian_4.jpeg)

![screenshot bagian 5](No_10_bagian_5.jpeg)

![screenshot bagian 6](No_10_bagian_6.jpeg)

![screenshot bagian 7](No_10_bagian_7.jpeg)

![screenshot bagian 8](No_10_bagian_8.jpeg)

![screenshot bagian 9](No_10_bagian_9.jpeg)

![screenshot bagian 10](No_10_bagian_10.jpeg)

![screenshot bagian 11](No_10_bagian_11.jpeg)

![screenshot bagian 12](No_10_bagian_12.jpeg)

![screenshot bagian 13](No_10_bagian_13.jpeg)

![screenshot bagian 14](No_10_bagian_14.jpeg)

![Nomor 10](No_10_Terbaru.gif)

