import java.util.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;


// Kelas utama TikTok
public class TikTok {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Scanner scLine = new Scanner(System.in);
        
// Mendefinisikan kelas Video
class Video {
    Scanner sc = new Scanner(System.in);
    Scanner scLine = new Scanner(System.in);
    private String title;
    private String description;
    private int duration;
    private String fileName;
    private User user;
    private int views;

    // Konstruktor untuk membuat objek Video
    public Video(String title, String description, int duration, String fileName, User user) {
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.fileName = fileName;
        this.user = user;
        this.views = 0;
    }

    // Metode untuk mengunggah video baru
    public void upload() {
        // Memeriksa apakah pengguna sudah masuk
        if (user.isLoggedIn()) {
            System.out.print("Video Title: ");
            title = scLine.nextLine();
            System.out.print("Video Description: ");
            description = scLine.nextLine();
            System.out.print("Video Duration: ");
            duration = sc.nextInt();
            System.out.print("File Name: ");
            fileName = scLine.nextLine();
            System.out.println("Uploading video: " + title);
            System.out.println("Video Description: " + description);
            System.out.println("Video Duration: " + duration);
            System.out.println("File Name: " + fileName);
            System.out.println("Uploaded by: " + user.getUsername());
        } else {
            System.out.println("User must be logged in to upload a video.");
        }
    }

    // Metode untuk mendapatkan judul video
    public String getTitle() {
        return title;
    }

    // Metode untuk mendapatkan deskripsi video
    public String getDescription() {
        return description;
    }

    // Metode untuk mendapatkan durasi video
    public int getDuration() {
        return duration;
    }

    // Metode untuk mendapatkan nama file video
    public String getFileName() {
        return fileName;
    }

    // Metode untuk mendapatkan pengguna yang mengunggah video
    public User getUser() {
        return user;
    }

    // Metode untuk memutar video
    public void play() {
        System.out.println("Playing video: " + getTitle());
        this.views++;
        System.out.println("Views: " + views);
    }

    // Metode untuk mendapatkan jumlah tampilan video
    public int getViews() {
        return views;
    }
}

// subclass TikTokVideo yang merupakan turunan dari superclass Video
class TikTokVideo extends Video {
    private int likes; // atribut likes
    private ArrayList<String> comments; // atribut comments
    private String effect; // atribut effect
    private double speed; // atribut speed
    private String filter; // atribut filter
    private boolean beautify; // atribut beautify
    private int startTime; // atribut startTime
    private int endTime; // atribut endTime
    private boolean TnJ; // atribut T&J
    private boolean mirror; // atribut cermin

    // constructor TikTokVideo dengan memanggil constructor superclass dan inisialisasi atribut likes, comments, effect, speed, filter, beautify, startTime, endTime, T&J, dan cermin
    public TikTokVideo(String title, String description, int duration, String fileName, User user) {
        super(title, description, duration, fileName, user);
        this.likes = 0;
        this.comments = new ArrayList<String>();
        this.effect = "";
        this.speed = 1.0;
        this.filter = "";
        this.beautify = false;
        this.startTime = 0;
        this.endTime = duration;
        this.TnJ = false;
        this.mirror = false;
    }

    // method untuk menambahkan like pada video
    public void like() {
        System.out.println("Liking video: " + getTitle());
        this.likes++;
        System.out.println("Likes: " + likes);
    }

    // method untuk menambahkan komentar pada video
    public void addComment(String comment) {
        System.out.println("Commenting on video: " + getTitle());
        this.comments.add(comment);
        System.out.println("Comments: " + comments);
    }

    // getter untuk atribut likes
    public int getLikes() {
        return likes;
    }

    // override method play dari superclass Video
    @Override
    public void play() {
        super.play();
        System.out.println("Effect: " + effect);
        System.out.println("Speed: " + speed);
        System.out.println("Filter: " + filter);
        System.out.println("Beautify: " + beautify);
        System.out.println("Start Time: " + startTime);
        System.out.println("End Time: " + endTime);
        System.out.println("T&J: " + TnJ);
        System.out.println("Mirror: " + mirror);
    }

    // setter untuk atribut effect
    public void setEffect(String effect) {
        this.effect = effect;
    }

    // setter untuk atribut speed
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    // setter untuk atribut filter
    public void setFilter(String filter) {
        this.filter = filter;
    }

    // setter untuk atribut beautify
    public void setBeautify(boolean beautify) {
        this.beautify = beautify;
    }

    // setter untuk atribut startTime
    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    // setter untuk atribut endTime
    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    // setter untuk atribut T&J
    public void setTnJ(boolean TnJ){
        this.TnJ = TnJ;
    }

     // setter untuk atribut Mirror
     public void setMirror(boolean Mirror){
        this.mirror = Mirror;
    }

    // getter untuk atribut comments
    public ArrayList<String> getComments() {
        return comments;
    }
}
// Mendefinisikan kelas LikeVideo
class LikeVideo {
    private boolean liked;
    User user = new User("lahitanissa", "satuduatigaempat", "nissacantik@gmail.com", "Anissa Lahitani", 1000000000 );
        
    // Konstruktor untuk membuat objek LikeVideo
    public LikeVideo(User user) {
        // Memeriksa apakah pengguna sudah masuk
        if (user.isLoggedIn()) {
            this.user = user;
            this.liked = false;
        } else {
            System.out.println("User must be logged in to like.");
        }
    }
        
    // Metode untuk menyukai video
    public void like(TikTokVideo video) {
        // Memeriksa apakah pengguna sudah menyukai video sebelumnya
        if (!liked) {
            video.like();
            liked = true;
        } else {
            System.out.println("Video already liked.");
        }
    }

    // Metode untuk memeriksa apakah video sudah disukai oleh pengguna
    public boolean isLiked() {
        return liked;
    }

    // Method untuk menghapus like pada video 
    public void removeLike(TikTokVideo video) {
        liked = false;
    }
}
// Mendefinisikan kelas KomentarVideo
class KomentarVideo {
    private User user;
    
    // Konstruktor untuk membuat objek KomentarVideo
    public KomentarVideo(User user) {
        // Memeriksa apakah pengguna sudah masuk
        if (user.isLoggedIn()) {
            this.user = user;
        } else {
            System.out.println("User must be logged in to comment.");
        }
    }

    // Metode untuk menambahkan komentar pada video
    public void comment(TikTokVideo video, String comment) {
        if (user != null) {
            video.addComment(comment);
        } else {
            System.out.println("User must be logged in to comment.");
        }
    }
}
// Mendefinisikan kelas SaveVideo
class SaveVideo {
    private boolean saved;
    User user = new User("lahitanissa", "satuduatigaempat", "nissacantik@gmail.com", "Anissa Lahitani", 1000000000 );
        
    // Konstruktor untuk membuat objek SaveVideo
    public SaveVideo(User user) {
        if (user.isLoggedIn()) {
            this.user = user;
            this.saved = false;
        } else {
            System.out.println("User must be logged in to save.");
        }
    }

    // Metode untuk menyimpan video
    public void save(Video video) {
        // Memeriksa apakah pengguna sudah menyimpan video sebelumnya
        if (!saved) {
            System.out.println("Saving video: " + video.getTitle());
            saved = true;
        } else {
            System.out.println("Video already saved.");
        }
    }

    // Metode untuk memeriksa apakah video sudah disimpan oleh pengguna
    public boolean isSaved() {
        return saved;
    }

    // Metode untuk menghapus save video 
    public void hapusSaveVideo(TikTokVideo video) {
        saved = false;
    }
}

// Mendefinisikan kelas ShareVideo
class ShareVideo {
    private User user;
    private ArrayList<String> sharedPlatforms; // Menyimpan daftar platform yang sudah digunakan untuk share

    // Konstruktor untuk membuat objek ShareVideo
    public ShareVideo(User user) {
        if (user.isLoggedIn()) {
            this.user = user;
            sharedPlatforms = new ArrayList<String>();
        } else {
            System.out.println("User must be logged in to share.");
        }
    }

    // Metode untuk membagikan video
    public void share(Video video) {
        if (user.isLoggedIn()) {
            Scanner input = new Scanner(System.in);
            String platform = "";
            
            while (!platform.equals("selesai")) {
                System.out.println("Select a platform to share the video:");
                System.out.println("1. WhatsApp");
                System.out.println("2. WhatsApp Status");
                System.out.println("3. Instagram");
                System.out.println("4. SMS");
                System.out.println("5. Email");
                System.out.println("6. Facebook");
                System.out.println("0. Exit");

                platform = input.nextLine();

                switch (platform) {
                    case "1":
                        shareVideoOnPlatform("WhatsApp", video);
                        break;
                    case "2":
                        shareVideoOnPlatform("WhatsApp Status", video);
                        break;
                    case "3":
                        shareVideoOnPlatform("Instagram", video);
                        break;
                    case "4":
                        shareVideoOnPlatform("SMS", video);
                        break;
                    case "5":
                        shareVideoOnPlatform("Email", video);
                        break;
                    case "6":
                        shareVideoOnPlatform("Facebook", video);
                        break;
                    case "0":
                        System.out.println("Exiting sharing menu.");
                        break;
                    default:
                        System.out.println("Invalid platform selected.");
                        break;
                }
            }
        } else {
            System.out.println("User must be logged in to share.");
        }
    }

    // Metode untuk membagikan video pada platform tertentu
    private void shareVideoOnPlatform(String platform, Video video) {
        if (!sharedPlatforms.contains(platform)) {
            System.out.println("Sharing video on " + platform + ": " + video.getTitle());
            sharedPlatforms.add(platform);
        } else {
            System.out.println("Video already shared on " + platform + ".");
        }
    }

    // Metode untuk mengecek apakah video sudah dibagikan di platform tertentu
    public boolean isVideoSharedOnPlatform(String platform) {
        return sharedPlatforms.contains(platform);
    }
}
// Mendefinisikan kelas JumlahTayangan
class JumlahTayangan {
    private TikTokVideo video;

    // Konstruktor untuk membuat objek JumlahTayangan
    public JumlahTayangan(TikTokVideo video) {
        this.video = video;
    }

    // Metode untuk menampilkan jumlah tayangan, jumlah like, dan komentar dari
    // video
    public void tayangan() {
        System.out.println("Jumlah tayangan video " + video.getTitle() + ": " + video.getViews());
        System.out.println("Jumlah like video: " + video.getLikes());
        System.out.println("Comments: " + video.getComments());
    }
}
class TikTokLive {
    // Atribut yang dimiliki oleh class TikTokLive
    private String title;
    private String description;
    private User user;
    private int viewers;
    private int heartCount;

    // Constructor untuk membuat objek TikTokLive
    public TikTokLive(String title, String description, User user) {
        this.title = title;
        this.description = description;
        this.user = user;
        this.viewers = 0;
        this.heartCount = 0;
    }

    // Method untuk memulai live streaming
    public void start() {
        if (user.isLoggedIn()) {
            System.out.println("Starting live: " + title);
            System.out.println("Live description: " + description);
        } else {
            System.out.println("User must be logged in to start a live.");
        }
    }

    // Method untuk menghentikan live streaming
    public void stop() {
        System.out.println("Stopping live: " + title);
    }

    // Method untuk bergabung dengan live streaming
    public void join() {
        System.out.println("Joining live: " + title);
        this.viewers++;
        System.out.println("Viewers: " + viewers);
    }

    // Method untuk memberikan hati ke live streaming
    public void giveHeart() {
        this.heartCount++;
        System.out.println("Giving heart to live: " + title);
        System.out.println("Heart count: " + heartCount);
    }

    // Getter untuk mengambil judul live streaming
    public String getTitle() {
        return title;
    }

    // Getter untuk mengambil deskripsi live streaming
    public String getDescription() {
        return description;
    }

    // Getter untuk mengambil pengguna yang membuat live streaming
    public User getUser() {
        return user;
    }

    // Getter untuk mengambil jumlah penonton yang sedang menonton live streaming
    public int getViewers() {
        return viewers;
    }

    // Getter untuk mengambil jumlah hati yang diberikan pada live streaming
    public int getHeartCount() {
        return heartCount;
    }
}

class Chat {
    private User sender;
    private User receiver;
    private String message;
    private LocalDateTime dateTime;

    public Chat(User sender, User receiver, String message) {
        // Memeriksa apakah pengguna sudah masuk
        if (sender.isLoggedIn()) {
            this.sender = sender;
            this.receiver = receiver;
            this.message = message;
            this.dateTime = LocalDateTime.now();
        } else {
            System.out.println("User must be logged in to send message.");
        }
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }

    public void sendMessage() {
        receiver.receiveMessage(this);
    }

    public void displayChat() {
        System.out.println("From: " + sender.getUsername());
        System.out.println("To: " + receiver.getUsername());
        System.out.println("Message: " + message);
        System.out.println("Date and Time: " + dateTime);
    }
}

class User {
    private String username;
    private String password;
    private boolean isLoggedIn;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.isLoggedIn = false;
    }

    public String getUsername() {
        return username;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void login(String password) {
        if (this.password.equals(password)) {
            isLoggedIn = true;
            System.out.println("User " + username + " is now logged in.");
        } else {
            System.out.println("Incorrect password. User " + username + " could not be logged in.");
        }
    }

    public void logout() {
        isLoggedIn = false;
        System.out.println("User " + username + " is now logged out.");
    }

    public void receiveMessage(Chat chat) {
        System.out.println("Received a message from " + chat.getSender().getUsername());
        System.out.println("Message: " + chat.getMessage());
    }
}

        class Saldo {
            private int saldo;
        
            public Saldo(int saldo) {
                this.saldo = saldo;
            }
        
            public int getSaldo() {
                return saldo;
            }
        
            public void setSaldo(int saldo) {
                this.saldo = saldo;
            }
        
            public void beli(int harga) {
                saldo -= harga;
            }
        
            public void jual(int harga) {
                saldo += harga;
            }
        }

        class Product {
            private String name;
            private int harga;
        
            public Product(String name, int harga) {
                this.name = name;
                this.harga = harga;
            }
        
            public String getName() {
                return name;
            }
        
            public int getHarga() {
                return harga;
            }
        }
        
class Pesanan {
    ArrayList<Product> items;
    private int totalHarga;

    public Pesanan() {
        items = new ArrayList<>();
        totalHarga = 0;
    }

    public void tambah(Product item) {
        items.add(item);
        totalHarga += item.getHarga();
    }

    public void hapus(Product item) {
        items.remove(item);
        totalHarga -= item.getHarga();
    }

    public int getTotalHarga() {
        return totalHarga;
    }

    public void bayar(Saldo saldo) {
        saldo.beli(totalHarga);
        items.clear();
        totalHarga = 0;
    }
}
class Voucher {
    private String code;
    private int value;

    public Voucher(String code, int value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public int getValue() {
        return value;
    }
} 

class Riwayat {
    private ArrayList<Pesanan> pesananList;

    public Riwayat() {
        pesananList = new ArrayList<>();
    }

    public void tambah(Pesanan pesanan) {
        pesananList.add(pesanan);
    }

    public void hapus(Pesanan pesanan) {
        pesananList.remove(pesanan);
    }

    public void tampilkan() {
        if (pesananList.size() == 0) {
            System.out.println("Tidak ada riwayat pesanan.");
            return;
        }
        System.out.println("Pesanan:");
        Pesanan pesananTerakhir = pesananList.get(pesananList.size() - 1);
        for (Product item : pesananTerakhir.items) {
            System.out.println(item.getName() + " - Rp" + item.getHarga());
        }
        System.out.println("Total harga: Rp" + pesananTerakhir.getTotalHarga());
    }

} 
public class TikTokShop {
    Scanner input = new Scanner(System.in);
    User user = new User("lahitanissa", "satuduatigaempat", "nissacantik@gmail.com", "Anissa Lahitani", 1000000000);
    Saldo saldo = new Saldo(user.getSaldo());
    Pesanan pesanan = new Pesanan();
    Riwayat riwayat = new Riwayat();

        public void Belanja(){
        
        int pilihan = 0;
        do {
            System.out.println("=== TIKTIKSHOP ===");
            System.out.println("Saldo Anda: Rp" + saldo.getSaldo());
            System.out.println("1. Beli produk");
            System.out.println("2. Tambah saldo");
            System.out.println("3. Lihat riwayat");
            System.out.println("4. Keluar");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();
            input.nextLine(); // membersihkan newline di buffer

            switch (pilihan) {
                case 1:
                    System.out.println("=== PRODUK ===");
                    Product product1 = new Product("Baju", 50000);
                    Product product2 = new Product("Celana", 70000);
                    Product product3 = new Product("Sepatu", 100000);
                    System.out.println("1. " + product1.getName() + " - Rp" + product1.getHarga());
                    System.out.println("2. " + product2.getName() + " - Rp" + product2.getHarga());
                    System.out.println("3. " + product3.getName() + " - Rp" + product3.getHarga());
                    System.out.print("Pilihan: ");
                    int produk = input.nextInt();
                    input.nextLine(); // membersihkan newline di buffer

                    Product selectedProduct = null;
                    switch (produk) {
                        case 1:
                            selectedProduct = product1;
                            break;
                        case 2:
                            selectedProduct = product2;
                            break;
                        case 3:
                            selectedProduct = product3;
                            break;
                        default:
                            System.out.println("Pilihan tidak valid.");
                            break;
                    }

                    if (selectedProduct != null) {
                        if (saldo.getSaldo() < selectedProduct.getHarga()) {
                            System.out.println("Saldo tidak mencukupi.");
                        } else {
                            pesanan.tambah(selectedProduct);
                            saldo.beli(selectedProduct.getHarga());
                            System.out
                                    .println("Produk " + selectedProduct.getName() + " telah ditambahkan ke pesanan.");
                            riwayat.tambah(pesanan); // Menambahkan pesanan ke objek Riwayat
                        }
                    }
                    break;
                case 2:
                    System.out.print("Masukkan jumlah saldo yang ingin ditambahkan: ");
                    int tambahSaldo = input.nextInt();
                    input.nextLine(); // membersihkan newline di buffer
                    saldo.jual(tambahSaldo);
                    System.out.println("Saldo berhasil ditambahkan.");
                    break;
                case 3:
                    System.out.println("=== RIWAYAT ===");
                    riwayat.tampilkan();
                    break;
                case 4:
                    System.out.println("Terima kasih telah berbelanja di TiktikShop.");
                    break;
                default:
                    System.out.println("Pilihan tidak valid.");
                    break;
            }

            System.out.println();
        } while (pilihan != 4);
    }
}
    }
}