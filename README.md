# Project TikTokPBO



## Source Code

```
import java.util.*;

class User {
    private String username;
    private String password;
    private String email;
    private boolean loggedIn;

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.loggedIn = false;
    }

    public boolean login(String username, String password) {
        if (this.username.equals(username) && this.password.equals(password)) {
            this.loggedIn = true;
            return true;
        } else {
            return false;
        }
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }
}

class Video {
    private String title;
    private String description;
    private int duration;
    private String fileName;
    private User user;

    public Video(String title, String description, int duration, String fileName, User user) {
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.fileName = fileName;
        this.user = user;
    }

    public void upload() {
        if (user.isLoggedIn()) {
            System.out.println("Uploading video: " + title);
            System.out.println("Uploaded by: " + user.getUsername());
        } else {
            System.out.println("User must be logged in to upload a video.");
        }
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getDuration() {
        return duration;
    }

    public String getFileName() {
        return fileName;
    }

    public User getUser() {
        return user;
    }
}

class TikTokVideo extends Video {
    private int likes;
    private int views;

    public TikTokVideo(String title, String description, int duration, String fileName, User user) {
        super(title, description, duration, fileName, user);
        this.likes = 0;
        this.views = 0;
    }

    public void play() {
        System.out.println("Playing video: " + getTitle());
        this.views++;
        System.out.println("Views: " + views);
    }

    public void like() {
        System.out.println("Liking video: " + getTitle());
        this.likes++;
        System.out.println("Likes: " + likes);
    }

    public int getLikes() {
        return likes;
    }

    public int getViews() {
        return views;
    }
}

public class TikTokUploader {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

            // Create a user
        User user = new User("lahitanissa", "1234", "lahitani@gmail.com");

    // Login
        System.out.println("Enter username: ");
        String username = scanner.nextLine();
        System.out.println("Enter password: ");
        String password = scanner.nextLine();
        if (user.login(username, password)) {
            System.out.println("Logged in as: " + user.getUsername());
        } else {
            System.out.println("Invalid username or password.");
            return;
        }

        // Create a video
        char lanjut;
        do {
            System.out.println("Enter video title: ");
            String title = scanner.nextLine();
            System.out.println("Enter video description: ");
            String description = scanner.nextLine();
            System.out.println("Enter video duration (in seconds): ");
            int duration = scanner.nextInt();
            scanner.nextLine(); // consume the remaining newline character
            System.out.println("Enter video file name: ");
            String fileName = scanner.nextLine();
            Video video = new Video(title, description, duration, fileName, user);

            // Upload the video
            video.upload();


            // Check if user wants to upload another video
            System.out.println("Upload another video? (y/n): ");
            lanjut = scanner.nextLine().charAt(0);

            
        } while (lanjut == 'y');

    

    }
}
            
```

